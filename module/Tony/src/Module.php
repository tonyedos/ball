<?php
/**
 * Created by PhpStorm.
 * User: Mr Tonny
 * Date: 03/02/2019
 * Time: 19:16
 */

namespace Tony;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
